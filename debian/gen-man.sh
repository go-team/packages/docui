#!/bin/bash

help2man \
    --help-option=-h \
    --version-string=" " \
    --no-discard-stderr \
    -n "TUI Client for Docker" \
    docui > debian/docui.1
